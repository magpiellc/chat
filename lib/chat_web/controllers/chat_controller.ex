defmodule ChatWeb.ChatController do
  use ChatWeb, :controller

  def chat(conn, %{"user" => user}) do
    render conn, "chat.html", user: user
  end
end
