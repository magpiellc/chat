defmodule ChatWeb.UserChannel do
  use ChatWeb, :channel

  def join("user:" <> _user, payload, socket) do
    if authorized?(payload) do
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  def handle_in("message:new", payload, socket) do
    ChatWeb.Endpoint.broadcast! "user:" <> payload["to"], "message:new", payload
    {:noreply, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end

end
