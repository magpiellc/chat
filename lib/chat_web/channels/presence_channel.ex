defmodule ChatWeb.PresenceChannel do
  use ChatWeb, :channel
  alias ChatWeb.Presence

  def join("presence", payload, socket) do
    if authorized?(payload) do
      send(self, :after_join)
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end

  def handle_info(:after_join, socket) do
    {:ok, _} = Presence.track(socket, socket.assigns.user,
      %{online_at: inspect(System.system_time(:seconds))})
    push socket, "presence_state", Presence.list(socket)
    {:noreply, socket}
  end
end
