# Chat and Presence for Blender

## Test

`
$ cd chat
$ iex -S mix phx.server
`

Then start 3 browser windows

http://0.0.0.0:4000/chat/bugs

http://0.0.0.0:4000/chat/daffy

http://0.0.0.0:4000/chat/porky

Now these 3 users can send one-on-one messages
