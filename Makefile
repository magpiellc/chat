brunch:
	if [ ! -d "./assets/node_modules" ]; \
        then \
	    cd assets && npm install; \
	fi

deps:
	mix deps.get

db:
	mix ecto.create ecto.migrate

setup: brunch deps db

live: setup
	iex -S mix phx.server

start: setup
	mix phx.server
