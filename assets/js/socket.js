// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "assets/js/app.js".

// To use Phoenix channels, the first step is to import Socket
// and connect at the socket path in "lib/web/endpoint.ex":
import {Socket, Presence} from "phoenix";

let socket = new Socket("/socket", {params: {token: window.userToken}});


// When you connect, you'll often need to authenticate the client.
// For example, imagine you have an authentication plug, `MyAuth`,
// which authenticates the session and assigns a `:current_user`.
// If the current user exists you can assign the user's token in
// the connection for use in the layout.
//
// In your "lib/web/router.ex":
//
//     pipeline :browser do
//       ...
//       plug MyAuth
//       plug :put_user_token
//     end
//
//     defp put_user_token(conn, _) do
//       if current_user = conn.assigns[:current_user] do
//         token = Phoenix.Token.sign(conn, "user socket", current_user.id)
//         assign(conn, :user_token, token)
//       else
//         conn
//       end
//     end
//
// Now you need to pass this token to JavaScript. You can do so
// inside a script tag in "lib/web/templates/layout/app.html.eex":
//
//     <script>window.userToken = "<%= assigns[:user_token] %>";</script>
//
// You will need to verify the user token in the "connect/2" function
// in "lib/web/channels/user_socket.ex":
//
//     def connect(%{"token" => token}, socket) do
//       # max_age: 1209600 is equivalent to two weeks in seconds
//       case Phoenix.Token.verify(socket, "user socket", token, max_age: 1209600) do
//         {:ok, user_id} ->
//           {:ok, assign(socket, :user, user_id)}
//         {:error, reason} ->
//           :error
//       end
//     end
//
// Finally, pass the token on connect as below. Or remove it
// from connect if you don't care about authentication.

socket.connect();

// Various channels clients use to communicate with the chat server
let user_channel = socket.channel("user:" +  window.userToken, {});
let presence_channel = socket.channel("presence", {user: window.userToken});

let msg_list = document.getElementById('msg-list');  // list of messages.
let sender = document.getElementById('from');  // name of message sender
let msg = document.getElementById('msg');  // message input field
let recipient = document.getElementById('to'); // select recipient
let presences = {}; // keep track of presence information

let renderUsers = (presences) => {
    recipient.innerHTML = Presence.list(presences, listUsers)
        .filter(presence => presence.user != window.userToken)
        .map(presence => `
             <option>${presence.user}</option>`).join("");
};

let listUsers = (user) => {
  return {
    user: user
  };
};

let addMessage = (from, message) => {
    var li = document.createElement("li");
    li.innerHTML = '<b>' + from + '</b>: ' + message;
    msg_list.appendChild(li);
};

let createMessage = (from, to, text) => {
   return {
       "from": from,
       "to": to,
       "body":{
           "content": text,
           "type":"text/plain",
           "fallback":"text : please upgrade your app to see this message"
       },
       "sender_time":1231412342313,
       "meta":{}
   };
};

user_channel.on('message:new', (payload) => {
    addMessage(payload.from, payload.message);
});

// "listen" for the [Enter] keypress event to send a message:
msg.addEventListener('keypress', (event) => {
    if (event.keyCode == 13 && msg.value.length > 0) { // don't sent empty msg.
        user_channel.push('message:new', {
            from: window.userToken,
            to : recipient.value,
            message: msg.value
        });
        addMessage("-> " + recipient.value, msg.value);
        msg.value = '';
  }
});

user_channel.join()
    .receive("ok",
             resp => {
                 console.log("Joined channel 'user' successfully", resp);
             })
    .receive("error",
             resp => {
                 console.log("Unable to join channel 'user'", resp);
             });

presence_channel.on('presence_state', state => {
    presences = Presence.syncState(presences, state);
    renderUsers(presences);
});

presence_channel.on('presence_diff', diff => {
    presences = Presence.syncDiff(presences, diff);
    renderUsers(presences);
});

presence_channel.join()
    .receive("ok",
             resp => {
                 console.log("Joined channel 'presence' successfully", resp);
             })
    .receive("error",
             resp => {
                 console.log("Unable to join channel 'presence'", resp);
             });

export default socket;
